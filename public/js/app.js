'use strict';

angular.module('mean', ['ngCookies', 'ngResource', 'ui.bootstrap', 'ui.router', 'mean.system', 'mean.projects', 'mean.issues', 'mean.users', 'localytics.directives','angularMoment', 'timeFilters'])
.run(function($rootScope) {
    $rootScope.user = window.user;
});

angular.module('mean.system', []);
angular.module('mean.projects', []);
angular.module('mean.issues', []);
angular.module('mean.users', []);