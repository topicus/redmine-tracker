'use strict';
angular.module('mean.users')
.controller('UsersController', ['$scope', '$stateParams', '$location', 'Global', 'Users', 'Issues','$timeout', '$http', '$element', 'filterFilter', 
    function ($scope, $stateParams, $location, Global, Users, Issues, $timeout, $http, $element, filterFilter) {
    $scope.global = Global;
    $scope.currentUser = Global;


    $scope.update = function(user) {
      var usr = $scope.currentUser;
      if (!usr.updated) {
        usr.updated = [];
      }
      usr.updated.push(new Date().getTime());

      usr.$update(function() {
          $location.path('/profile');
      });
    };
    $scope.findOne = function() {
        Users.get({
            userId: window.user._id
        }, function(u) {
            $scope.currentUser = u;
        });
    };  

}]);