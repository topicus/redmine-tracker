'use strict';
angular.module('mean.issues')
.controller('IssuesController', ['$scope', '$stateParams', '$location', 'Global', 'Issues', '$timeout', '$http', 'Projects', '$element', 'filterFilter', 
    function ($scope, $stateParams, $location, Global, Issues, $timeout, $http, Projects, $element, filterFilter) {
    $scope.global = Global;
    $scope.counter = 0;
    $scope.globalCounter = 0;
    $scope.activeIssue = null;
    $scope.activeProject = null;
    $scope.playing = false;
    $scope.journals = {};
    $scope.moment = moment;
    $scope.inputNotes = {};
    $scope.cursor = -1;
    $scope.humanFieldnames = {
        'assigned_to_id' : {
            'setted':'Asignado a <%=new_value%>',
            'changed':'Asignado ha cambiado  <%=old_value%> por <%=new_value%>'
        },
        'priority_id' : {
            'setted':'Prioridad ha sido establecido a <%=new_value%>',
            'changed':'Prioridad ha cambiado  <%=old_value%> por <%=new_value%>'
        },
        'fixed_version_id' : {
            'setted':'Version prevista ha sido establecido a <%=new_value%>',
            'changed':'Prioridad ha cambiado  <%=old_value%> por <%=new_value%>'
        },
        'attachment' : {
            'setted':'Añadido fichero <%=new_value%>'
        },
        'estimated_hours':{
            'setted':'Tiempo estimado ha sido establecido a <%=new_value%>',
            'changed':'Tiempo estimado ha cambiado  <%=old_value%> por <%=new_value%>'
        },
        'due_date':{
            'setted':'Fecha límite ha sido establecido a <%=new_value%>',
            'changed':'Fecha límite ha cambiado  <%=old_value%> por <%=new_value%>'
        },
        'parent_id':{
            'setted':'Tarea padre ha sido establecida a <%=new_value%>',
            'changed':'Tarea padre ha cambiado  <%=old_value%> por <%=new_value%>'
        },
        'status_id':{
            'setted':'Estado ha sido establecida a <%=new_value%>',
            'changed':'Estado ha cambiado  <%=old_value%> por <%=new_value%>'
        }
    };
    $scope.$on('keypress', processKeyEvent);
    window.onbeforeunload = onExit;

    var timeout = null;
    var filteredArray = [];
    var cache = {};
    
    function processKeyEvent(e, data){

        var list = filteredArray || $scope.issues;
        var $search = $('#searchInput');
        var $target = $(data.target);

        //arriba
        if(data.keyCode === 38 && !$target.parent().hasClass('chosen-search')){
          if($scope.cursor === -1 || $scope.cursor === 0){
            $scope.cursor = list.length - 1;
          } else{
            $scope.cursor = $scope.cursor - 1;
          }
          $search.blur();
        }
        
        //abajo
        if(data.keyCode === 40 && !$target.parent().hasClass('chosen-search')){
          if($scope.cursor === -1 || $scope.cursor === (list.length -1)){
            $scope.cursor = 0;
          } else{
            $scope.cursor = $scope.cursor + 1;
          }
          $search.blur();
        }

        if(data.keyCode == 27 && ($target[0].tagName !== 'INPUT' || $target[0].tagName !== 'TEXTAREA')){
            $target.blur();
        }

        if(data.keyCode === 32 && $scope.cursor !== -1){
            $scope.issues[$scope.cursor].expanded = !$scope.issues[$scope.cursor].expanded;
        }

        if($target.attr('id') !== 'searchInput'){
          if(data.keyCode === 13){
            if($scope.playing && $scope.activeIssue.id == $scope.issues[$scope.cursor].id){
              $scope.stop($scope.activeIssue);
            }else if($scope.cursor !== -1){
              var issue = $scope.issues[$scope.cursor];
              $scope.play(issue);
            }
            $search.blur();
          }
          if(data.keyCode === 83 && $target[0].tagName !== 'INPUT' && $target[0].tagName !== 'TEXTAREA'){
            $search.focus();
          }
        }
      
    }

    function onExit() {
        if($scope.playing){
            return "A task is currently running";
        } else {
            return null;
        }            
    }

    function showAlert(message){
        var imageUrl = '/img/icons/' + message.level + '.png';
        var notification = window.webkitNotifications.createNotification(imageUrl, message.title, message.body);
        notification.show();
    }

    function checkForMessages(){
        if($scope.counter && $scope.counter % (60 * 60 * 2) === 0){
            showAlert({level: 'info', title: 'Alerta de tiempo', body:'Estas trabajando hace ' + secondsToTimeString($scope.counter) +' en la misma tarea.'});
        }
    }

    function recomputeSelect(){        
        console.log('hola');
        // if(typeof filteredArray !== 'undefined' 
        //     && filteredArray.length 
        //     && ($scope.issues.length !== filteredArray.length)){
         
        //     $scope.cursor = 0;
        // }else {
        //     $scope.cursor = -1;
        // }
    }

    function capitaliseFirstLetter(string){
        return string.charAt(0).toUpperCase() + (string.slice(1)).toLowerCase();
    };

    function findIssueIndexById(issueId){
        for (var i = $scope.issues.length - 1; i >= 0; i--) {
            if($scope.issues[i]._id === issueId) return i;
        }
    }
    function checkForNewTasks(){
        if($scope.globalCounter % (60 * 2) === 0){
            var item = _.findWhere($scope.issues, {expanded: true});
            if (!item) {
                $scope.find();
            };
        }
    }

        
    function secondsToTimeString(input) {
        var totalSec = (input)? input : 0;
        var hours = parseInt( totalSec / 3600 ) % 24;
        var minutes = parseInt( totalSec / 60 ) % 60;
        var seconds = totalSec % 60;
        return (hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds  < 10 ? '0' + seconds : seconds);
    };

    function eventLoop() {
        $scope.globalCounter++;
        checkForMessages();
        checkForNewTasks();
    };
    setInterval(eventLoop, 1000);

    $scope.$watch('search',function(newValue){
       filteredArray = filterFilter($scope.issues, newValue);
       recomputeSelect();
    });

    $scope.onTimeout = function(){
        $scope.counter++;
        timeout = $timeout($scope.onTimeout, 1000);
    };

    $scope.toggle = function(issue){
        console.log('TOGGLE');
        if($scope.playing){
            $scope.stop(issue);
        } else {
            $scope.play(issue);
        }
    };
    
    $scope.play = function(issue){
        if($scope.playing)
            $scope.stop($scope.activeIssue);
        $scope.activeIssue = issue;
        timeout = $timeout($scope.onTimeout, 1000);
        $scope.playing = true;
        $scope.search = null;
    };

    $scope.stop = function(issue){
        console.log('STOP');
        $scope.playing = false;
        var issueIndex = findIssueIndexById($scope.activeIssue._id);
        if($scope.issues[issueIndex])
            $scope.issues[issueIndex].spent_time = $scope.issues[issueIndex].spent_time  + $scope.counter;
        $scope.activeIssue = null;
        var timeEntry = {issue_id: issue.id, seconds: $scope.counter, hours: ($scope.counter/60)/60, activity_id:'9', comments:''};
        $http({
            method:'POST',
            url: '/issues/add_time',
            data: timeEntry,
        })
        .then(function(result) {
            console.log(result);
        }, function(error) {
            console.log(error);
        });
        $scope.counter = 0;
        $timeout.cancel(timeout);

    };

    $scope.onEnterSearch = function(event){
        if(filteredArray && event.keyCode === 13){
            if(filteredArray.length){
                $scope.play(filteredArray[0]);
            } else if(filteredArray == 0) {
                $element.find('.chosen-container').trigger('mousedown');
                $element.find('.chosen-search input').focus();
            }
        }
    };
    
    $scope.onEnterProject = function(){
        console.log('enter project');
        $scope.create();
    };

    $scope.create = function() {
        var subject = $element.find('#searchInput').val();
        if($scope.activeProject && subject !== ''){
            var issueData = {
                'tracker_id':2,
                'project_id':$scope.activeProject,
                'status_id':1,
                'subject': subject,
                'priority_id':4,
                'assigned_to_id': window.user.rid
            };

            var issue = new Issues(issueData);
            issue.$save(function(response) {
                $scope.issues.splice(0, 0,issue);
                $scope.play(response);
            });
        }
    };

    $scope.toggleDetails = function(issueId){
        var issueIndex = findIssueIndexById(issueId);
        if($scope.issues[issueIndex]._id === issueId){
            if($scope.issues[issueIndex].expanded){
                $scope.issues[issueIndex].expanded = false;
            }else{
                $scope.issues[issueIndex].expanded = true;
                $scope.findOne(issueId);
            }
        }
    };

    $scope.prevent = function($event){
        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.remove = function(issue) {
        if (issue) {
            issue.$remove();

            for (var i in $scope.issues) {
                if ($scope.issues[i] === issue) {
                    $scope.issues.splice(i, 1);
                }
            }
        }
        else {
            $scope.issue.$remove();
            $location.path('issues');
        }
    };

    $scope.update = function(_issue) {
        var issue = {issue:{}};
        var notes = $scope.inputNotes[_issue._id];
        if(notes){
            issue.issue.id = _issue.id;
            issue.issue.notes = notes;
        }
        
        $http({
            method:'PUT',
            url: '/issues',
            data: issue,
        })
        .then(function(result) {
            $scope.findOne(_issue._id);
            $scope.inputNotes[_issue._id] = '';
        }, function(error) {
            console.log(error);
            $scope.inputNotes[_issue._id] = '';
        });
        
    };
    $scope.closeTask = function(_issue) {
        var issue = {issue:{}};
        var notes = $scope.inputNotes[_issue._id];
        if(notes){
            issue.issue.notes = notes;
        }
        if($scope.activeIssue && $scope.activeIssue.id === issue.issue.id && $scope.playing){
            $scope.stop($scope.activeIssue);
            $scope.activeIssue = null;
        }
        issue.issue.id = _issue.id;
        issue.issue.assigned_to_id = _issue.author.id; //back to the author
        issue.issue.status_id = 5; //status of closed tasks
        issue.issue.done_ratio = 100; //status of closed tasks
        
        $http({
            method:'PUT',
            url: '/issues',
            data: issue,
        })
        .then(function(result) {
            $scope.find();
            $scope.inputNotes[_issue._id] = '';
        }, function(error) {
            $scope.inputNotes[_issue._id] = '';
        });
    }
    $scope.find = function() {
        if($scope.user){
            $scope.findProjects();
            Issues.query(function(issues) {
                [].map.call(issues, function(issue){
                    issue.subject = capitaliseFirstLetter(issue.subject);
                    return issue;
                });
                $scope.issues = issues;
            });
        }
    };

    $scope.findOne = function(issueId) {
        Issues.get({
            issueId: issueId
        }, function(issue) {
            $scope.issue = issue;
            $scope.issue.expanded = true;
            _.map(issue.journals, function(j){
                if(!_.isUndefined(j.details[0])){
                    var d = j.details[0];
                    if(d && $scope.humanFieldnames[d.name]){
                        var tpl = $scope.humanFieldnames[d.name][(d.old_value)?'changed':'setted'];
                        var message = _.template(tpl, d);
                        j.message = message;
                        return j;
                    }
                    
                }
            });
            $scope.journals[issueId] = issue.journals;
        });
    };

    $scope.findProjects = function() {
        if($scope.user){
            Projects.query(function(projects) {
                $scope.projects = projects;
            });
        }
    };


    $scope.findOneProject = function() {
        Projects.get({
            projectId: $stateParams.projectId
        }, function(project) {
            $scope.project = project;
        });
    };

}]);