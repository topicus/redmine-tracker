'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {

    // For unmatched routes:
    $urlRouterProvider.otherwise('/');

    // states for my app
    $stateProvider
      .state('all issues', {
        url: '/issues',
        templateUrl: 'views/issues/list.html'
    })
      .state('create issue', {
        url: '/issues/create',
        templateUrl: 'views/issues/create.html'
    })
      .state('edit issue', {
        url: '/issues/:issueId/edit',
        templateUrl: 'views/issues/edit.html'
    })
      .state('issue by id', {
        url: '/issues/:issueId',
        templateUrl: 'views/issues/view.html'
    })
      .state('profile', {
        url: '/profile',
        templateUrl: 'views/users/profile.html'
    })
      .state('edit profile', {
        url: '/profile/edit',
        templateUrl: 'views/users/edit.html'
    })         
      .state('home', {
        url: '/',
        templateUrl: 'views/issues/list.html'
    });      
}
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
  function($locationProvider) {
    $locationProvider.hashPrefix('!');
}
]);
