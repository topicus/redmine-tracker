'use strict';

//Issues service used for issues REST endpoint
angular.module('mean.users').factory('Users', ['$resource', function($resource) {
    return $resource('users/:userId', {
        userId: '@_id'
    }, {
        update: {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'}
        }
    });
    
}]);