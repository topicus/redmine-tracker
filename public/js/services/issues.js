'use strict';

//Issues service used for issues REST endpoint
angular.module('mean.issues').factory('Issues', ['$resource', function($resource) {
    return $resource('issues/:issueId', {
        issueId: '@_id'
    }, {
        update: {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'}
        }
    });
}]);

//Issues service used for issues REST endpoint
angular.module('mean.issues').factory('Projects', ['$resource', function($resource) {
    return $resource('projects/:projectId', {
        projectId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);