'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Issue = mongoose.model('Issue');

//Globals
var user;
var issue;

//The tests
describe('<Unit Test>', function() {
    describe('Model Issue:', function() {
        beforeEach(function(done) {
            user = new User({
                name: 'Full name',
                email: 'test@test.com',
                username: 'user',
                password: 'password'
            });

            user.save(function() {
                issue = new Issue({
                    title: 'Issue Title',
                    content: 'Issue Content',
                    user: user
                });

                done();
            });
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                return issue.save(function(err) {
                    should.not.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without title', function(done) {
                issue.title = '';

                return issue.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });

        afterEach(function(done) {
            Issue.remove({});
            User.remove({});
            done();
        });
        after(function(done) {
            Issue.remove().exec();
            User.remove().exec();
            done();
        });
    });
});