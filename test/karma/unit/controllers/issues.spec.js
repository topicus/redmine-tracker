'use strict';

(function() {
    // Issues Controller Spec
    describe('MEAN controllers', function() {
        describe('IssuesController', function() {
            // The $resource service augments the response object with methods for updating and deleting the resource.
            // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
            // the responses exactly. To solve the problem, we use a newly-defined toEqualData Jasmine matcher.
            // When the toEqualData matcher compares two objects, it takes only object properties into
            // account and ignores methods.
            beforeEach(function() {
                this.addMatchers({
                    toEqualData: function(expected) {
                        return angular.equals(this.actual, expected);
                    }
                });
            });

            // Load the controllers module
            beforeEach(module('mean'));

            // Initialize the controller and a mock scope
            var IssuesController,
                scope,
                $httpBackend,
                $stateParams,
                $location;

            // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
            // This allows us to inject a service but then attach it to a variable
            // with the same name as the service.
            beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {

                scope = $rootScope.$new();

                IssuesController = $controller('IssuesController', {
                    $scope: scope
                });

                $stateParams = _$stateParams_;

                $httpBackend = _$httpBackend_;

                $location = _$location_;

            }));

            it('$scope.find() should create an array with at least one issue object ' +
                'fetched from XHR', function() {

                    // test expected GET request
                    $httpBackend.expectGET('issues').respond([{
                        title: 'An Issue about MEAN',
                        content: 'MEAN rocks!'
                    }]);

                    // run controller
                    scope.find();
                    $httpBackend.flush();

                    // test scope value
                    expect(scope.issues).toEqualData([{
                        title: 'An Issue about MEAN',
                        content: 'MEAN rocks!'
                    }]);

                });

            it('$scope.findOne() should create an array with one issue object fetched ' +
                'from XHR using a issueId URL parameter', function() {
                    // fixture URL parament
                    $stateParams.issueId = '525a8422f6d0f87f0e407a33';

                    // fixture response object
                    var testIssuesData = function() {
                        return {
                            title: 'An Issue about MEAN',
                            content: 'MEAN rocks!'
                        };
                    };

                    // test expected GET request with response object
                    $httpBackend.expectGET(/issues\/([0-9a-fA-F]{24})$/).respond(testIssueData());

                    // run controller
                    scope.findOne();
                    $httpBackend.flush();

                    // test scope value
                    expect(scope.issue).toEqualData(testIssueData());

                });

            it('$scope.create() with valid form data should send a POST request ' +
                'with the form input values and then ' +
                'locate to new object URL', function() {

                    // fixture expected POST data
                    var postIssueData = function() {
                        return {
                            title: 'An Issue about MEAN',
                            content: 'MEAN rocks!'
                        };
                    };

                    // fixture expected response data
                    var responseIssueData = function() {
                        return {
                            _id: '525cf20451979dea2c000001',
                            title: 'An Issue about MEAN',
                            content: 'MEAN rocks!'
                        };
                    };

                    // fixture mock form input values
                    scope.title = 'An Issue about MEAN';
                    scope.content = 'MEAN rocks!';

                    // test post request is sent
                    $httpBackend.expectPOST('issues', postIssueData()).respond(responseIssueData());

                    // Run controller
                    scope.create();
                    $httpBackend.flush();

                    // test form input(s) are reset
                    expect(scope.title).toEqual('');
                    expect(scope.content).toEqual('');

                    // test URL location to new object
                    expect($location.path()).toBe('/issues/' + responseIssueData()._id);
                });

            it('$scope.update() should update a valid issue', inject(function(Issues) {

                // fixture rideshare
                var putIssueData = function() {
                    return {
                        _id: '525a8422f6d0f87f0e407a33',
                        title: 'An Issue about MEAN',
                        to: 'MEAN is great!'
                    };
                };

                // mock issue object from form
                var issue = new Issues(putIssueData());

                // mock issue in scope
                scope.issue = issue;

                // test PUT happens correctly
                $httpBackend.expectPUT(/issues\/([0-9a-fA-F]{24})$/).respond();

                // testing the body data is out for now until an idea for testing the dynamic updated array value is figured out
                //$httpBackend.expectPUT(/issues\/([0-9a-fA-F]{24})$/, putIssueData()).respond();
                /*
                Error: Expected PUT /issues\/([0-9a-fA-F]{24})$/ with different data
                EXPECTED: {"_id":"525a8422f6d0f87f0e407a33","title":"An Issue about MEAN","to":"MEAN is great!"}
                GOT:      {"_id":"525a8422f6d0f87f0e407a33","title":"An Issue about MEAN","to":"MEAN is great!","updated":[1383534772975]}
                */

                // run controller
                scope.update();
                $httpBackend.flush();

                // test URL location to new object
                expect($location.path()).toBe('/issues/' + putIssueData()._id);

            }));

            it('$scope.remove() should send a DELETE request with a valid issueId' +
                'and remove the issue from the scope', inject(function(Issues) {

                    // fixture rideshare
                    var issue = new Issues({
                        _id: '525a8422f6d0f87f0e407a33'
                    });

                    // mock rideshares in scope
                    scope.issues = [];
                    scope.issues.push(issue);

                    // test expected rideshare DELETE request
                    $httpBackend.expectDELETE(/issues\/([0-9a-fA-F]{24})$/).respond(204);

                    // run controller
                    scope.remove(issue);
                    $httpBackend.flush();

                    // test after successful delete URL location issues lis
                    //expect($location.path()).toBe('/issues');
                    expect(scope.issues.length).toBe(0);

                }));
        });
    });
}());