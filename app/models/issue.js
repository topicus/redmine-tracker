'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IssueSchema =  new Schema({
    spent_time: {type:Number,default:0},
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    is_new: { type: Boolean, default: true },
    description: String,
    attachments: [{
        filesize: Number,
        content_url: String,
        author: {
            id: Number,
            name: String
        },
        created_on: Date,
        description: String,
        filename: String,
        id: Number,
        content_type: String
    }],
    journals: [{
        notes: String,
        created_on: Date,
        user: {
            name: String,
            id: Number
        },
        id: Number,
        details:[{
            name: String,
            new_value: String,
            property: String,
            old_value: String
        }]
    }],
    created_on: Date,
    author: {
        name: String,
        id: Number
    },
    updated_on: Date,
    assigned_to: {
        name: String,
        id: Number
    },
    status: {
        id: Number,
        name: String
    },
    done_ratio: Number,
    tracker: {
        id: Number,
        name: String
    },
    id: Number,
    subject: String,
    priority:{
        id: Number,
        name: String
    },
    project: {
        name: String,
        id: Number
    },
    start_date: {type: Date, default: Date.now}
});
mongoose.model('Issue', IssueSchema);