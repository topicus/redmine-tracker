'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProjectSchema =  new Schema({
    id: Number,
    description: String,
    custom_fields: [{id: Number, name: String, value:String}],
    created_on: Date,
    updated_on: Date,
    identifier: String,
    name: String,
    parent: {id:Number, name:String}
});
mongoose.model('Project', ProjectSchema);