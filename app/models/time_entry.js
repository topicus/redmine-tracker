'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TimeEntrySchema =  new Schema({ 
  id: Number,
  project: {
    name:String, 
    id:Number
  },
  issue: Number,
  user: {
    name:String, 
    id:Number
  },
  hours: Number,
  comments: String,
  spent_on: Date,
  created_on: Date,
  updated_on: Date,
});
var TimeEntry = mongoose.model('TimeEntry', TimeEntrySchema);