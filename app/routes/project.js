'use strict';

// Projects routes use project controller
var projects = require('../controllers/projects');
var authorization = require('./middlewares/authorization');

// project authorization helpers
var hasAuthorization = function(req, res, next) {
  if (req.project.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    User.findById(req.user.id, function(err, user){
        res.locals.redmine = new Redmine(user.rserver, user.api_key);
        next();
    });    
};

module.exports = function(app) {
    app.get('/projects',  authorization.requiresLogin, projects.all);
};