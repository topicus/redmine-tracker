'use strict';

// Issues routes use issues controller
var issues = require('../controllers/issues');
var authorization = require('./middlewares/authorization');
var Redmine = require('redmine-api');
var mongoose = require('mongoose'),
    User = mongoose.model('User');
// issue authorization helpers
var hasAuthorization = function(req, res, next) {
	if (!req.user.id) {
        return res.redirect('/');
    }
    User.findById(req.user.id, function(err, user){
        console.log('USER ID: ' + req.user.id);
        console.log('USER: ' + user);

        res.locals.user = user;
        res.locals.redmine = new Redmine(user.rserver, user.api_key);
        next();
    });    
};

module.exports = function(app) {
    app.post('/issues/add_time', hasAuthorization, authorization.requiresLogin, issues.addTime);

    app.get('/issues', hasAuthorization, authorization.requiresLogin, issues.all);
    app.post('/issues', hasAuthorization, authorization.requiresLogin, issues.create);
    app.get('/issues/:issueId', hasAuthorization, authorization.requiresLogin, issues.show);
    app.put('/issues', hasAuthorization, authorization.requiresLogin, issues.update);
    app.del('/issues/:issueId', hasAuthorization, authorization.requiresLogin, issues.destroy);
    // Finish with setting up the issueId param
    app.param('issueId', hasAuthorization, issues.issue);

};