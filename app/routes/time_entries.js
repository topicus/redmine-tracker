'use strict';

// TimeEntries routes use timeEtries controller
var timeEntries = require('../controllers/time_entries');
var authorization = require('./middlewares/authorization');


var loadRedmine = function(req, res, next) {
    if (!req.body.rserver || !req.body.api_key) {
        return res.redirect('/');
    }
    res.locals.redmine = new Redmine(req.body.rserver, req.body.api_key);
    next();
};

module.exports = function(app) {
    app.post('/time_entries/add_time', loadRedmine, authorization.requiresLogin, timeEntries.addTime);
    app.get('/time_entries/by_project', authorization.requiresLogin, timeEntries.computeTimeByProject);
};