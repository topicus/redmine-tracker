'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Project = mongoose.model('Project'),
    Redmine = require('redmine-api'),
    moment = require('moment'),
    Q = require('q'),
    _ = require('lodash');


exports.all = function (req, res) {
    Project.find().sort('-updated_on').populate('user', 'name username').exec(function(err, projects) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(projects);
        }
    });  
};


function sync(cb){
    var redmine = new Redmine('http://redmine.buenosaires.gob.ar','883475b5c3ada34d6236a89496f45a5f422d1ddf');
    redmine.api('projects', {numRows: -1}, function(err, projects) {
        Project.find().exec(function(err, _projects){
            var promises = [],
                promise = null;
            
            _.each(_projects, function(project){
                if(!_.findWhere(projects,{id:project.id})){
                    promise = Project.findOneAndRemove({id:project.id},{}, function(err, res){
                        console.log(err);
                    });
                    promises.push(promise);
                }
            });

            _.each(projects, function(project){
                promise = Project.findOneAndUpdate({id:project.id}, project, {upsert:true}, function(err, result){
                    console.log(err, result);
                });
                promises.push(promise);
            });
            if(_.isFunction(cb)){
                Q.all(promises).then(cb(projects));
            }
        });
    });
}
setInterval(sync, 5000 * 60);