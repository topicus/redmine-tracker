'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Issue = mongoose.model('Issue'),
    TimeEntry = mongoose.model('TimeEntry'),
    User = mongoose.model('User'),
    Redmine = require('redmine-api'),
    moment = require('moment'),
    Q = require('q'),
    _ = require('lodash');
    

function sync(req, res, cb){
    res.locals.redmine.api('issues', {numRows: 100, assigned_to_id: 'me', status_id:'opened', project_id:'dgid', include:'journals,children,changesets'}, function(err, issues) {
        Issue.find().exec(function(err, _issues){
            var promises = [],
                promise = null;
            
            _.each(_issues, function(issue){
                if(!_.findWhere(issues,{id:issue.id})){
                    promise = Issue.findOneAndRemove({id:issue.id},{}, function(err, res){
                        console.log(err);
                    });
                    promises.push(promise);
                }
            });

            _.each(issues, function(issue){
                var exists = _.findWhere(_issues, {id:issue.id});
                issue.is_new = exists ? exists.is_new : true;
                issue.user = req.user._id;
                issue.start_date = moment(issue.start_date).unix();
                promise = Issue.findOneAndUpdate({id:issue.id}, issue, {upsert:true}, function(err, result){
                    console.log(err);
                });
                promises.push(promise);
            });
            Q.all(promises).then(cb(issues));
        });
    });
}

/**
 * Find issue by id
 */
exports.issue = function(req, res, next, id) {
    Issue.findById(id, function(err, issue) {
        if(!err && issue && issue.id){
            res.locals.redmine.api('issues/' + issue.id, {include:'journals,children,changesets'}, function(err, issue) {
                if (err) return next(err);
                if (!issue) return next(new Error('Failed to load issue ' + id));
                req.issue = issue[0];
                Issue.findOneAndUpdate({id: req.issue.id}, {is_new:false}, function(err, result){
                    console.log(err);
                    next();
                });
            });
        }else{
            next();
        }
    });
};

/**
 * Create an issue
 */
exports.create = function(req, res) {
    console.log('CALL CREATE');
    var issue = { issue:req.body};
    res.locals.redmine.api('issues', {method:'POST', body: issue}, function(err, _issue){
        var issue = new Issue(_issue.issue);
        issue.is_new = true;
        issue.start_date = moment(_issue.start_date).unix();
        issue.user = req.user._id;
        issue.save(function(err) {
            console.log(err);
            if (err) {
                return res.send('users/signup', {
                    errors: err.errors,
                    issue: issue
                });
            } else {
                res.jsonp(issue);
            }
        });
    });
};
// PUT /issues/[id].xml
// <?xml version="1.0"?>
// <issue>
//   <subject>Subject changed</subject>
//   <notes>The subject was changed</notes>
// </issue>
/**
 * Update an issue
 */
exports.update = function(req, res) {
    console.log('CALL UPDATE');
    res.locals.redmine.api('issues/' + req.body.issue.id, {method:'PUT', body: {issue:req.body.issue}}, function(err, status){
        console.log(req.body.issue);
        Issue.findOneAndUpdate({id: req.body.issue.id}, req.body.issue, function(err, result){
            console.log(err, result);
            if(err) res.jsonp({status:'error'});
            res.jsonp(result);
        });
    });
};

/**
 * Delete an issue
 */
exports.destroy = function(req, res) {
    console.log('CALL DESTROY');
    Issue.findById(req.params.issueId, function(err, issue){
        res.locals.redmine.api('issues/' + issue.id, {method:'DELETE'}, function(err, _issue){
            console.log(err, _issue);
        });
    }).remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                issue: issue
            });
        } else {
            res.jsonp({status:'OK'});
        }
    });
};

/**
 * Show an issue
 */
exports.show = function(req, res) {
    console.log('CALL SHOW');
    res.jsonp(req.issue);
};

/**
 * List of Issues
 */
exports.all = function(req, res) {
    console.log('CALL ALL');
    sync(req, res, function(){
        Issue.find().sort('-updated_on').populate('user', 'name username').exec(function(err, issues) {
            if (err) {
                res.render('error', {
                    status: 500
                });
            } else {
                res.jsonp(issues);
            }
        });
    });
};

/**
 * Add time to issue
 */
exports.addTime = function(req, res) {
    Issue.findOneAndUpdate({id:req.body.issue_id}, { $inc: { spent_time: req.body.seconds}}, function(err, issue){
        res.locals.redmine.api('time_entries', {method:'POST', body:{ time_entry:req.body}}, function(err, time_entry){
            var timeEntry = new TimeEntry(time_entry.time_entry);
            timeEntry.save();
            res.jsonp({status:'OK'});
        });
    });
};

