'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    TimeEntry = mongoose.model('TimeEntry'),
    Issue = mongoose.model('Issue'),
    User = mongoose.model('User'),
    Redmine = require('redmine-api'),
    moment = require('moment'),
    Q = require('q'),
    _ = require('lodash');


/**
 * Add time entry
 */
exports.addTime = function(req, res) {
    Issue.findOneAndUpdate({id:req.body.issue_id}, { $inc: { spent_time: req.body.seconds}}, function(err, issue){
        res.locals.redmine.api('time_entries', {method:'POST', body:{ time_entry:req.body}}, function(err, time_entry){
            var timeEntry = new TimeEntry(time_entry.time_entry);
            timeEntry.save();
            res.jsonp({status:'OK'});
        });
    });
};

exports.computeTimeByProject = function(req, res){
  var weekStart = moment().startOf('week').unix();
  var weekEnd = moment().endOf('week').unix();

  console.log("WEEK START: " + moment().startOf('week').unix());
  console.log("WEEK END: " + moment().endOf('week').unix());

  TimeEntry.find(
    {
      update_on:{"$gte": weekStart, "$lte": weekEnd} 
    }, function(err, entries){
      if(err) res.send(500, err);
      res.jsonp(entries);
    }
  )
};