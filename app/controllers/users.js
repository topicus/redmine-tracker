'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    _ = require('underscore');

/**
 * Auth callback
 */
exports.authCallback = function(req, res) {
    res.redirect('/');
};

/**
 * Show login form
 */
exports.signin = function(req, res) {
    res.render('users/signin', {
        title: 'Signin',
        message: req.flash('error')
    });
};

/**
 * Show sign up form
 */
exports.signup = function(req, res) {
    res.render('users/signup', {
        title: 'Sign up',
        user: new User()
    });
};

/**
 * Show edit form
 */
exports.update = function(req, res, next) {
    var user = req.profile;

    user = _.extend(user, req.body);

    user.save(function(err) {
        if (err) {
            return res.send(500, {err: err.toString()});
        } else {
            res.jsonp(user);
        }
    });
};


/**
 * Logout
 */
exports.signout = function(req, res) {
    req.logout();
    res.redirect('/');
};

/**
 * Session
 */
exports.session = function(req, res) {
    res.redirect('/');
};

/**
 * Create user
 */
exports.create = function(req, res, next) {
    var user = new User(req.body);
    var message = null;
    user.provider = 'local';
    res.locals.redmine.api('users/current', function(err, ruser){
        if(err) res.send(500);
        
        var ruser = ruser[0];
        user.rid = ruser.id;
        user.email = ruser.mail;
        user.username = user.email;
        user.name = ruser.firstname + ' ' + ruser.lastname;
        console.log(user);
        user.save(function(err) {
            if (err) {
                switch (err.code) {
                    case 11000:
                    case 11001:
                        message = 'Username already exists';
                        break;
                    default:
                        message = 'Please fill all the required fields';
                }

                return res.render('users/signup', {
                    message: message,
                    user: user
                });
            }
            req.logIn(user, function(err) {
                if (err) return next(err);
                return res.redirect('/');
            });
        });

    });
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

/**
 * Find user by id
 */
exports.get = function(req, res, next) {
    res.jsonp(req.user);
};
/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
    User
    .findOne({
        _id: id
    })
    .exec(function(err, user) {
        if (err) return next(err);
        if (!user) return next(new Error('Failed to load User ' + id));
        req.profile = user;
        next();
    });
};