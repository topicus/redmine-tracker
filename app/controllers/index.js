'use strict';

exports.render = function(req, res) {
		console.log('USUARIO: ' + req.user);
		if(req.user){
	    res.render('index', {
	        user: req.user ? JSON.stringify(req.user) : 'null'
	    });
		}else{
			res.redirect('/signin');
		}
};
